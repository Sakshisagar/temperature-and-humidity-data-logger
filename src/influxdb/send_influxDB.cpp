/** @file send_influxDB.cpp
 *  @brief send the temperature and humidity data along with timestamp to InfluxDB
 *
 *	This program parese the json file containing influxdb configuration details
 *  and sends the temperature and humidity values to the influxdb using
 *  Shunya Interfaces API
 *
 *  @author Shriram K
 *  @bug  When compiled in Shunya OS docker container with
 *		  command: $ "g++ problem1.cxx -lshunyaInterfaces" There comes an
 *  	  ERROR: "was not declared in this scope" for shunyaInterfaces API functions
 */

/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *  This block contains initialization code for this particular file.
 *#####################################################################
 */

/* --- Standard Includes --- */
#include <iostream>
#include <stdlib.h>
#include <string.h>


/* --- Project Includes --- */
#include <shunyaInterfaces.h>
#include "include/rapidjson/document.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/filereadstream.h"
#include "include/rapidjson/filewritestream.h"
#include "include/rapidjson/prettywriter.h"

/* using standard namespace */
using namespace std;
/* using rapidjson namespace */
using namespace rapidjson;

/* MACROS */
#define DB_URL_SIZE			255
#define DB_NAME_SIZE		255
#define FILE_NAME			"config_influxdb.json"

/*--- Function Prototypes ---*/
void parse_file(void);
void influxdb_init(void);
void influxdb_send(float t, float h, long double timestamp);


/* --- Global Variables --- */
/* Declare a JSON document. JSON document parsed will be stored in this variable */
Document d;

/* JSON Object that stores the influx configuration details in json file */
Value& influxdb_config;



/*
 *#####################################################################
 *  Process block
 *  -------------
 *
 *#####################################################################
 */

/**
 *  @brief parse_file()
 *
 *  parses the influx db config json file and stores it in document
 *
 *  @return nothing
 */

void parse_file(void)
{

	/* Open the json file in read mode */
    FILE* fp = fopen(FILE_NAME, "r");

	/* Declare read buffer */
    char readBuffer[65536];

    /* Declare stream for reading the example stream */
    FileReadStream is(fp, readBuffer, sizeof(readBuffer));

    /* Parse example.json and store it in `d` */
	d.ParseStream(is);

    /* Close the example.json file*/
    fclose(fp);

}



/**
 *  @brief influxdb_init()
 *
 *  initializes the struct InfluxdbSettings
 *
 *  @return nothing
 */
void influxdb_init(void)
{

	/* store the influxdb config details in this global json object variable */
	influxdb_config = d["test-db"];

	/* NOTE:This variable will be used in influxdb_send to create a
	 * new influxdb instance and send the data
	 */

	


}

/**
 *  @brief influxdb_send
 *
 *  sends the temperature and humidity data along with their timestamps
 *  to influxdb
 *
 *  @aruguments temperature and humidity data
 *
 *  @return nothing
 */
void influxdb_send(float t, float h, long double timestamp)
{
	/* Create a new instance */
	influxdbObj db1 = newInfluxdb(influxdb_config);
	/* Send the temperature data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement temperature=%d %ld", t, unix_timestamp);
	/* Send the humidity data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement humidity=%d %ld", float, unix_timestamp);
}


/**
 *  @brief main function
 *
 *  main entry point of the program
 *
 *  @return 0
 */
int main()
{
	/* Dummy values to test code */
	float temp, humid;
	long double time;

	/* Parse the json file */
	parse_file();
	/* initialize the Influx_DB API */
	influxdb_init();
	/* Send the data to InfluxDB database */
	influxdb_send(temp, humid, time);

	return 0;
}
