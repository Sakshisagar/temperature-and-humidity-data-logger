/** @file wifi_code.cxx
 *  @configure the wifi parameter and connect/disconnect
 *  
 *  Parses config.json file and configures wifi connection using the parameters provided in the .json file
 *  
 *  @author Shriram K @co-author Anurag singh 
 *  @bug 
 *		
 *		 NOTE: Program compiles with command "g++ wifi_code.cxx" works but it is not tested with 
 *	     cmake build commands
 */	     

/*
 *#####################################################################
 *  Instruction to test the program
 *  -------------------------------
 *  Change only the values in the provided test.json file for example: 
 *  change the values of status,ssid,password,etc for wifi;
 *
 *#####################################################################
 */

/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *
 *#####################################################################
 */

/* --- Standard Includes --- */
#include <cstdio>
#include <iostream>
#include <cstdlib>

/* --- Project Includes --- */
#include "include/rapidjson/document.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/filereadstream.h"
#include "include/rapidjson/filewritestream.h"
#include "include/rapidjson/prettywriter.h"

/* using standard namespace */
using namespace std; 
/* using rapidjson namespace */
using namespace rapidjson;

/* MACROS */
#define FILE_NAME "config_wifi.json"

/* Declare a JSON document. JSON document parsed will be stored in this variable */
Document d;

/* Function Prototypes */
void parse_file(void);
void set_wifi(void);

/*
 *#####################################################################
 *  Process block
 *  -------------
 *  
 *#####################################################################
 */

/** 
 *  @brief parse_file()
 *  
 *  parses the json file and stores it in document
 *
 *  @return nothing
 */

void parse_file(void)
{
	
	/* Open the example.json file in read mode */
        FILE* fp = fopen(FILE_NAME, "r"); 

        /* Declare read buffer */
        char readBuffer[65536];

        /* Declare stream for reading the example stream */
        FileReadStream is(fp, readBuffer, sizeof(readBuffer));


        /* Parse example.json and store it in `d` */
        d.ParseStream(is);

        /* Close the example.json file*/
        fclose(fp);
	
}

/** 
 *  @brief set_wifi()
 *  
 *  connects/disconnects wifi
 *
 *  @return nothing
 */

void set_wifi(void)
{
    /* Declare an object to store the  "wireless" "device" value */
	Value& wdevice = d["wireless"]["device"];
    /* Declare an object to store the  "wireless" "status" value */
	Value& wstatus = d["wireless"]["status"];

	/* string to store status value */
	string check = wstatus.GetString();

    /* check if the status value is on */
	if(check.compare("on") == 0){
                /* Declare an object to store the  "wireless" "ssid" value */
	        Value& wssid = d["wireless"]["ssid"];
                /* Declare an object to store the  "wireless" "password" value */
	        Value& wpass = d["wireless"]["password"];	
                /* String to store the ssid */
	        string ssid = wssid.GetString();
                /* String to store the password */
	        string pass =  wpass.GetString();

                /* send command to turn on the network manager */
	        system("nmcli n on");
                /* start the wpa_supplicatn.service */
	        system("sudo systemctl start wpa_supplicant.service");
                /* append the given ssid and password with the command*/
	        string str = "nmcli dev wifi connect "+ ssid +" "+"password "+ pass;
                /* convert the command string to const char type */       
	        const char *command = str.c_str();
                /* pass the command to the shell */
	        system(command);
	}

        /* check if the status value is off */
        else if(check.compare("off") == 0){
                /* turn off the network manager */
	        system("nmcli radio wifi off");
	}
	/* Print the message to the user */
	cout<<"------------------------------------------------------------------"<<endl;
	cout<<"                WIFI SETTINGS APPLIED                             "<<endl;
	cout<<"------------------------------------------------------------------"<<endl;

}

/** 
 *  @brief main function
 *  
 *  main entry point of the program
 *
 *  @return nothing 
 */

int main (void)
{
	/* Parse the json file */
    parse_file( );

    /* configure the wifi */
	set_wifi();

    return 0;
}
