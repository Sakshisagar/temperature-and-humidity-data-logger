/** @file store_to_JSON.c
 *  @brief stores the sensor data in json file format
 *  
 *  This program stores the temperature and humidity data that is read from a DHT22 Sensor in a .json file
 *  
 *  @author Harmin Naik
 *
 */


/* --- Standard Includes --- */
#include <shunyaInterfaces.h>

#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#define _POSIX_C_SOURCE 200112L
#define MAX_TIMINGS	85
#define DHT_PIN		3	/* GPIO-22 */
int data[5] = { 0, 0, 0, 0, 0 };


/* --- Function Prototypes --- */
void store_sensor_data(float,float);

/* MACROS */
#define FILE_NAME "/home/harmeeen/Study/Internships/THDL/Project/JSON Files/sensor_data.json"  //Insert intended file location

/* --- Global Variables --- */
FILE *fptr;

void store_sensor_data(float c, float h)
{
    
    //Open the file
    fptr = fopen(FILE_NAME,"a");
    
    if(fptr == NULL)
    {
        printf("Error!");   
        exit(1);             
    }
    
    setenv("TZ", "/usr/share/zoneinfo/Asia/Calcutta", 1);
    struct tm tm = *localtime(&(time_t){time(NULL)});    

    fprintf(fptr,"{\n");
    fprintf(fptr,"\t\"time_stamp\": \"%s\",",asctime(&tm));
    fprintf(fptr,"\n");
    fprintf(fptr,"\t\"temperature\": \"%f\",",c);
    fprintf(fptr,"\n");
    fprintf(fptr,"\t\"humidity\": \"%f\"",h);
    fprintf(fptr,"\n");
    fprintf(fptr,"}\n");
    
    //Close the file
    fclose(fptr);
}

void read_dht_data()
{
    uint8_t laststate	= HIGH;
    uint8_t counter	= 0;
    uint8_t j		= 0, i;


    data[0] = data[1] = data[2] = data[3] = data[4] = 0;

    /* pull pin down for 18 milliseconds */
    pinMode( DHT_PIN, OUTPUT );
    digitalWrite( DHT_PIN, LOW );
    delay( 18 );

    /* prepare to read the pin */
    pinMode( DHT_PIN, INPUT );

    /* detect change and read data */
    for ( i = 0; i < MAX_TIMINGS; i++ )
    {
        counter = 0;
        while ( digitalRead( DHT_PIN ) == laststate )
        {
            counter++;
            delayMicroseconds( 1 );
            if ( counter == 255 )
            {
                break;
            }
        }
        laststate = digitalRead( DHT_PIN );
        if ( counter == 255 )
        	break;
        /* ignore first 3 transitions */

    	if ( (i >= 4) && (i % 2 == 0) )
	{
        /* shove each bit into the storage bytes */
        	data[j / 8] <<= 1;
    		if ( counter > 16 )
        	data[j / 8] |= 1;
        	j++;
    	}
    } 

/*
* check we read 40 bits (8bit x 5 ) + verify checksum in the last byte
 * print it out if data is good
*/
 	if ( (j >= 40) && (data[4] == ( (data[0] + data[1] + data[2] + data[3]) & 0xFF) ) )
 	{
     		float h = (float)((data[0] << 8) + data[1]) / 10;
     		if ( h > 100 )
     		{	
        	h = data[0];	// for DHT11
     		}
     		float c = (float)(((data[2] & 0x7F) << 8) + data[3]) / 10;
     		if ( c > 125 )
     		{
        	c = data[2];	// for DHT11
     		}
     		if ( data[2] & 0x80 )
     		{
    			c = -c;
     		}
	}
     	store_sensor_data(c,h);    
}

int main()
{
       initLib();
       sprintf( "Raspberry Pi DHT11/DHT22 temperature/humidity test\n" );
        if ( wiringPiSetup() == -1 )
        	exit( 1 );
        while ( 1 )
        {
            read_dht_data();
            delay( 2000 ); /* wait 2 seconds before next read */
         }
  return(0);
}



