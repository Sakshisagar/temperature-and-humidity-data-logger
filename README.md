# Temperature and Humidity Data logger[THDL]
THDL project monitors the temperature and relative humidity data in a warehouse environment to ensure that the goods are stored in the optimal storage conditions thereby increasing the product's shelf life resulting in increased profits.

## Project Architecture Diagram

![arch.img](extras/THDL_Arch_v2.1.jpg)

## Project Status
- [Project Planning Sheet](https://docs.google.com/spreadsheets/d/1OtPkqQpqGmR7TIBb6dmRJkMfcYd6KHXqNkk7eu2fIQo/edit#gid=0) 

## Skills Required
1. C/C++
2. Basics of Linux
3. IoT
4. HTML/CSS
5. Documentation Skills
6. Team Player

## Rewards for Contributions

```First, Thank you for deciding to contribute to THDL Project. We really appreciate it.```

-  Contributors who stay and **contribute till the end of the project will be felicitated with certificates!**

Also by contibuting to this project you will **gain the following skills:**
- Experience working on an industry grade project
- Get acquaited with How open-source project works
- Gain/Improve domain specific IoT skills by implementing major features in the project

## How to Contribute?

Please read [CONTRIBUTING.md](https://gitlab.com/iotiotdotin/project-internship/temperature-and-humidity-data-logger/-/blob/master/CONTRIBUTING.md) for complete details on how to contribute to this project 

## Current Active Contributors

- [Anurag Singh(@agangwar908)](https://gitlab.com/agangwar908)
- [Shivarth Chourey(@shivarthpiyu)](https://gitlab.com/shivarthpiyu)

## Current Maintainer

- [Anurag Singh(@agangwar908)](https://gitlab.com/agangwar908)

## Prerequisites

If you are going to contribute to **code**(adding features,bug fixes,**testing**,etc) then following are the prerequisites for it:
- [Ubuntu 16.04](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/installUbuntu)
- [Docker Engine](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/dockerBasics#docker-installation)
- [Shunyaos Docker Guide](https://hub.docker.com/r/shunyaos/shunya-armv7)

## Reporting Bugs
If you find any bugs/mistakes anywhere in this project,Please report them in this Issue[ #38](https://gitlab.com/iotiotdotin/project-internship/temperature-and-humidity-data-logger/-/issues/38)

## Discussion
If you have questions/doubts or want to make suggestions, Please use the issue [#40](https://gitlab.com/iotiotdotin/project-internship/temperature-and-humidity-data-logger/-/issues/40)

