
**Lithium-ion Battery**


| specification | detail |
| ------ | ------ |
|  Capacity（25±5℃） | Nominal Capacity： 2600mAh (0.52A Discharge，2.75V) Typical Capacity：2550mAh（0.52A Discharge，2.75V） Minimum Capacity：2500mAh (0.52ADischarge，2.75V) |
|  Nominal Voltage | 3.7V  |
| Internal Impedance |≤ 70mΩ|
| Discharge Cut-off Voltage |3.0V |
| Max Charge Voltage |4.20±0.05V |
| Standard Charge Current |0.52A|
|Rapid Charge Current |1.3A |
| Standard Discharge Current|0.52A |
|Rapid Discharge Current |1.3A |
|Max Pulse Discharge Current |2.6A |
|Weight |46.5±1g |
|Max. Dimension |Diameter(Ø): 18.4mm Height (H): 65.2mm |
|Operating Temperature |Charge: 0 ~ 45℃ Discharge: -20 ~ 60℃ |
| Storage Temperature |During 1 month: -5 ~ 35℃ During 6 months: 0 ~ 35℃ |


more detail [battery specification](https://www.ineltro.ch/media/downloads/SAAItem/45/45958/36e3e7f3-2049-4adb-a2a7-79c654d92915.pdf)

we can use Lithium-ion Battery in series and parallel combination for our requirement .