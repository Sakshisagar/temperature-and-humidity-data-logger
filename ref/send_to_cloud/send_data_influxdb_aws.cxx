/** @file problem1.cpp
 *  @brief Solution to problem1
 *  
 *  This program parses configuration json file, reads the data from the Rish_Master_3430        *  meter via modbus and sends the data to aws and influx db using ShunyaOS Interfaces API    
 *
 * @Note This code requires RapidJSON library to parse the JSON files
 *  
 *  @author Shriram K
 *  @bug  When compiled in Shunya OS docker container with 
 *		  command: $ "g++ problem1.cxx -lshunyaInterfaces" There comes an 
 *  	  ERROR: "was not declared in this scope" for shunyaInterfaces API functions
 */

/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *  This block contains initialization code for this particular file.
 *#####################################################################
 */

/* --- Standard Includes --- */
#include <iostream>
#include <stdlib.h>
#include <string.h>

/* --- Project Includes --- */
#include <shunyaInterfaces.h>
#include "include/rapidjson/document.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/filereadstream.h"
#include "include/rapidjson/filewritestream.h"
#include "include/rapidjson/prettywriter.h"

/* using standard namespace */
using namespace std; 
/* using rapidjson namespace */
using namespace rapidjson;

/* MACROS */
#define TOPIC_SIZE	 		255 
#define MESSAGE_LENGTH 	 	255
#define HOST_ADDRESS_SIZE	255
#define DB_URL_SIZE			255
#define DB_NAME_SIZE		255

/* --- Global Variables --- */
/* modbus Device Name */
char dev[255];
/* modbus communication baud rate */
uint32_t BAUD;
/* timestamp to be sent to influx db */
long double unix_timestamp;
/* Declare a JSON document. JSON document parsed will be stored in this variable */
Document d; 


/* This structure contains all the information required for aws connection */
struct AWSSettings {

  /* MQTT HOST URL */
  char hostURL[HOST_ADDRESS_SIZE];
  /* MQTT port */
  uint32_t port;
  /* Cert location */
  char certDir[PATH_MAX + 1];
  /* Client Root CA file name */
  char rootCA[PATH_MAX + 1];
  /* Client certificate file name */
  char certName[PATH_MAX + 1];
  /* Client Private Key file name */
  char privKey[PATH_MAX + 1];
  /* Quality Of Service 0 or 1 */
  uint8_t QOS;
  /* Client ID */
  char clientID[255];

}aws1;

/* This structure contains information about Influx DB */
struct InfluxdbSettings { 
	/* Database URL */
        char dbURL[DB_URL_SIZE];
	/* Database Name */
        char dbName[DB_NAME_SIZE]; 
}db1;

/*
 *#####################################################################
 *  Process block
 *  -------------
 *  
 *#####################################################################
 */

/** 
 *  @brief parse_file()
 *  
 *  parses the json file and stores it in document
 *
 *  @return nothing
 */

void parse_file(void)
{
	
	/* Open the example.json file in read mode */
        FILE* fp = fopen("example.json", "r"); 

        /* Declare read buffer */
        char readBuffer[65536];

        /* Declare stream for reading the example stream */
        FileReadStream is(fp, readBuffer, sizeof(readBuffer));


        /* Parse example.json and store it in `d` */
        d.ParseStream(is);

        /* Close the example.json file*/
        fclose(fp);
	
}


/** 
 *  @brief modbus_init()
 *  
 *  initializes the modbus variables like device name
 *  and baud rate
 *
 *  @return nothing
 */
void modbus_init()
{
	/* Declare an object to store the  modbus device name */
	Value& modbus_dev = d["settings"][ "modbus"]["device"];
	/* Copy the device name string to the the variable */
	strncpy(dev, modbus_dev.GetString(), 255);

	/* Declare an object to store the  modbus baud rate */
	Value& modbus_baud = d["settings"][ "modbus"][ "baud"];
	/* Convert the Baud rate from string to integer type and store it */
	BAUD = (uint32_t)atoi(modbus_baud.GetString());


}

/** 
 *  @brief aws_init()
 *  
 *  initializes the struct AWSSettings 
 *
 *  @return nothing
 */
void aws_init()
{
	/* Print the message */
	cout<<"Configuring AWS Settings"<<endl;	


	/* Configure the AWS settings from the JSON File  */

	/* Store the aws hostUrl in an object */
	Value& sett_aws_host = d["settings"][ "aws"]["hostURL"];
	/* Copy the hostURL string to struct AWSSettings variable */
	strncpy(aws1.hostURL, sett_aws_host.GetString(), HOST_ADDRESS_SIZE);
	
	/* Store the aws_port in an object */
	Value& sett_aws_port = d["settings"][ "aws"]["port"];
	/* Convert the aws_port from string to integer data type and store it */
	uint32_t port = (uint32_t)atoi(sett_aws_port.GetString());
	/* Assign the port value to the struct variable */
	aws1.port = port;
	
	/* Store the aws certificate directory in an object */
	Value& sett_aws_certdir = d["settings"][ "aws"]["certDir"];
	/* Copy the aws certificate directory string to struct AWSSettings variable */
	strncpy(aws1.certDir, sett_aws_certdir.GetString(), PATH_MAX + 1);
	
	/* Store the aws rootca in an object */
	Value& sett_aws_rootca = d["settings"][ "aws"]["rootCA"];
	/* Copy the aws rootca string to struct AWSSettings variable */
	strncpy(aws1.rootCA, sett_aws_rootca.GetString(), PATH_MAX + 1);

	/* Store the aws Certificate name in an object */
	Value& sett_aws_certname = d["settings"][ "aws"]["certName"];
	/* Copy the aws certificate name string to struct AWSSettings variable */
	strncpy(aws1.certName, sett_aws_certname.GetString(), PATH_MAX + 1);
	
	/* Store the aws private key in an object */
	Value& sett_aws_pkey = d["settings"][ "aws"]["privKey"];
	/* Copy the aws private key string to struct AWSSettings variable */
	strncpy(aws1.privKey, sett_aws_pkey.GetString(), PATH_MAX + 1);

	/* Store the aws Client ID in an object */
	Value& sett_aws_client = d["settings"][ "aws"][ "clientID"];
	/* Copy the aws Client ID string to struct AWSSettings variable */
	strncpy(aws1.clientID, sett_aws_client.GetString(), 255);

	/* Store the aws QOS in an object */
	Value& sett_aws_qos = d["settings"][ "aws"][ "QOS"];
	/*  Convert the aws QOS from string to integer data type and store it */
	uint8_t QOS = (uint8_t)atoi(sett_aws_qos.GetString());
	/* Assign the port value to the struct variable */
	aws1.QOS = QOS;

	/* variable to store the AWS Status Code */
	uint32_t status;
	/* Print message */
	cout<<"Connecting to AWS Cloud"<<endl;	
	/* Connects to the AWS IoT Cloud using given credentials */
	status = awsConnectMqtt(aws1);

}

/** 
 *  @brief aws_send_data()
 *  sends the data to aws cloud
 * 
 *  @param[in] topic  MQTT Topic to publish
 *  @param[in] message Message that needs to be published
 *
 *  @return nothing
 */
void aws_send_data(char topic[],char message[] )
{
	/* variable to store the AWS Status Code */
	uint32_t status;
	/* publish the data to aws cloud */
	status = awsPublishMqtt(topic, message);
	/* Check the response */
	if(status == 200){
		cout<<"OK"<<endl;
	}// end of if
	else{
		/* Print error message */
		cout<<"ERROR in sending data to AWS"<<endl;

	}// end of else
}

/** 
 *  @brief influxdb_init()
 *  
 *  initializes the struct InfluxdbSettings
 *
 *  @return nothing
 */
void influxdb_init()
{
	/* Print the message */
	cout<<"Configuring Influx DB Settings"<<endl;	

	/* Configure the InfluxDB settings from the JSON File */
	
	/* Store the influx db_url in an object */
	Value& sett_db_url = d["settings"][ "influxdb"]["db_url"];
	/* Copy the db_url string to struct InfluxdbSettings variable */
	strncpy(db1.dbURL, sett_db_url.GetString(), DB_URL_SIZE);

	/* Store the influx db_name in an object */
	Value& sett_db_name = d["settings"][ "influxdb"]["db_name"];
	/* Copy the db_name string to struct InfluxdbSettings variable */
	strncpy(db1.dbName, sett_db_name.GetString(),DB_NAME_SIZE);


}

/** 
 *  @brief read_voltage()
 *  
 *  reads the voltage from the the meter via modbus 
 *  and sends the data to AWS Cloud and Influx DB
 *
 *  @return nothing
 */
void read_voltage()
{
	/* address of the registers to be read for v1, v2, v3 */
	uint16_t addr[] = {30001, 30003, 30005};
	/* variable to store the read data */
	int ret;
	/* variable to store the MQTT Topic */
	char topic[TOPIC_SIZE];
	/* variable to store the value to be sent */
	char message[MESSAGE_LENGTH];

	/* Read the voltage data and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[0]);
	/* Convert the voltage value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"volts1");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement volts1=%d %ld", ret, unix_timestamp);

	/* Read the voltage data and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[1]);
	/* Convert the voltage value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"volts2");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement volts2=%d %ld", ret, unix_timestamp);

	/* Read the voltage data and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[2]);
	/* Convert the voltage value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"volts3");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement volts3=%d %ld", ret, unix_timestamp);
}

/** 
 *  @brief read_current()
 *  
 *  reads the current from the the meter via modbus 
 *  and sends the data to AWS Cloud and Influx DB
 *
 *  @return nothing
 */
void read_current()
{
	/* address of the registers to be read for i1, i2, i3 */
	uint16_t addr[] = {30007, 30009, 30011};
	/* variable to store the read data */
	int ret;
	/* variable to store the MQTT Topic */
	char topic[TOPIC_SIZE];
	/* variable to store the value to be sent */
	char message[MESSAGE_LENGTH];

	/* Read the current data and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[0]);
	/* Convert the current value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"current1");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement current1=%d %ld", ret, unix_timestamp);

	/* Read the current data and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[1]);
	/* Convert the current value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"current2");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement current2=%d %ld", ret, unix_timestamp);

	/* Read the current data and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[2]);
	/* Convert the current value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"current3");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement current3=%d %ld", ret, unix_timestamp);

}

/** 
 *  @brief read_power()
 *  
 *  reads the power data from the the meter via modbus 
 *  and sends the data to AWS Cloud and Influx DB
 *
 *  @return nothing
 */
void read_power()
{
	/* Adddress of the register to be read from */
	uint16_t addr = 30051;
	/* variable to store the read data */
	int ret;
	/* variable to store the MQTT Topic */
	char topic[TOPIC_SIZE];
	/* variable to store the value to be sent */
	char message[MESSAGE_LENGTH];

	/* Read the power data and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr);
	/* Convert the current value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"active_power");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement active_power=%d %ld", ret, unix_timestamp);

}

/** 
 *  @brief read_app_energy()
 *  
 *  reads the apparent energy value from the the meter via modbus 
 *  and sends the data to AWS Cloud and Influx DB
 *
 *  @return nothing
 */
void read_app_energy()
{
	/* Adddress of the register to be read from */
	uint16_t addr = 30081;
	/* variable to store the read data */
	int ret;
	/* variable to store the MQTT Topic */
	char topic[TOPIC_SIZE];
	/* variable to store the value to be sent */
	char message[MESSAGE_LENGTH];

	/* Read the apparent energy and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr);
	/* Convert the apparent energy value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"apparent_energy");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement apparent_energy=%d %ld", ret, unix_timestamp);
}

/** 
 *  @brief read_react_energy()
 *  
 *  reads the reactive energy value from the the meter via modbus 
 *  and sends the data to AWS Cloud and Influx DB
 *
 *  @return nothing
 */
void read_react_energy()
{
	/* address of the registers to be read from */
	uint16_t addr[] = {30077, 30079};
	/* variable to store the read data */
	int ret;
	/* variable to store the MQTT Topic */
	char topic[TOPIC_SIZE];
	/* variable to store the value to be sent */
	char message[MESSAGE_LENGTH];

	/* Read the reactive energy(import) and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[0]);
	/* Convert the reactive energy(import) value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"reactive_energy_import");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement reactive_energy_import=%d %ld", ret, unix_timestamp);

	/* Read the reactive energy(export) and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[1]);
	/* Convert the reactive energy(export) value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"reactive_energy_export");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement reactive_energy_export=%d %ld", ret, unix_timestamp);

}

/** 
 *  @brief read_act_energy()
 *  
 *  reads the active energy value from the the meter via modbus 
 *  and sends the data to AWS Cloud and Influx DB
 *
 *  @return nothing
 */
void read_act_energy()
{
	/* address of the registers to be read from */
	uint16_t addr[] = {30073, 30075};
	/* variable to store the read data */
	int ret;
	/* variable to store the MQTT Topic */
	char topic[TOPIC_SIZE];
	/* variable to store the value to be sent */
	char message[MESSAGE_LENGTH];

	/* Read the active energy(import) and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[0]);
	/* Convert the reactive energy(import) value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"active_energy_import");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement active_energy_import=%d %ld", ret, unix_timestamp);

	/* Read the active energy(export) and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[1]);
	/* Convert the active energy(export) value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"active_energy_export");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement active_energy_export=%d %ld", ret, unix_timestamp);
}

/** 
 *  @brief read_vthd()
 *  
 *  reads the voltage THD value from the the meter via modbus 
 *  and sends the data to AWS Cloud and Influx DB
 *
 *  @return nothing
 */
void read_vthd()
{
	/* address of the registers to be read from */
	uint16_t addr[] = {30207, 30209, 30211};
	/* variable to store the read data */
	int ret;
	/* variable to store the MQTT Topic */
	char topic[TOPIC_SIZE];
	/* variable to store the value to be sent */
	char message[MESSAGE_LENGTH];

	/* Read the v1 thd and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[0]);
	/* Convert the v1 thd value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"v1thd");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement v1_thd=%d %ld", ret, unix_timestamp);

	/* Read the v2 thd and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[1]);
	/* Convert the v2 thd value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */
	strcpy(topic,"v2thd");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement v2_thd=%d %ld", ret, unix_timestamp);

	/* Read the v3 thd and store it in variable */
	ret = modbusRTURead(dev, BAUD, addr[2]);
	/* Convert the v3 thd value from integer to string */
	sprintf(message,"%d",ret);
	/* Store the converted string in variable topic */	
	strcpy(topic,"v3thd");
	/* Send the data to aws cloud */
	aws_send_data(topic,message);
	/* Send the data+timestamp to Influx DB */
	writeDbInflux (db1, "measurement v3_thd=%d %ld", ret, unix_timestamp);
}


/** 
 *  @brief main function
 *  
 *  main entry point of the program
 *
 *  @return 0 
 */
int main()
{	
	/* Parse the json file */
	parse_file();
	/* initialize the modbus API */
	modbus_init();
	/* initialize the AWS API */
	aws_init();
	/* initialize the Influx_DB API */
	influxdb_init();
	/* read the Voltage data and send the data */
	read_voltage();
	/* read the current data and send the data */
	read_current();
	/* read the power data and send the data */
	read_power();
	/* read the apparent energy and send the data */
	read_app_energy();
	/* read the reactive energy and send the data */
	read_react_energy();
	/* read the active energy and send the data */
	read_act_energy();
	/* read the Voltage THD data and send the data */
	read_vthd();

	return 0;
}
